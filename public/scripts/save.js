turnOnSaveBtn();

// Funcionalidade do botão de salvar
function turnOnSaveBtn() {

    const saveBtn = document.getElementById('save-btn');
    saveBtn.addEventListener('click', save);
}


// Formata uma string adequadamente para servir como nome de arquivo.
// Substitui espaços por - ou _ por exemplo.
function formatToFileName(name) {

    let fileName = name.replace(' ', '_');
    return fileName;
}


// Salvas as informações de uma ficha
function save() {

    // Cria o blob, algo como arquivo temporário no browser
    // Dados da página princial
    const basicInfo = saveBasicInfo();
    const attrInfo = saveAttributes();
    const dispInfo = saveDispInfo();
    const capInfo = saveCapInfo();
    const statsInfo = saveStats();
    const woundInfo = saveWounds();
    const skillsInfo = saveSkills();

    // Dados da página de conhecimentos e traços
    const specialSkillsInfo = saveSpecialSkills();
    


    const sheetInfo = { basicInfo: basicInfo, attrInfo: attrInfo, 
                        dispInfo: dispInfo, statsInfo: statsInfo,
                        capInfo: capInfo, woundInfo: woundInfo,
                        skillsInfo: skillsInfo, 
                        specialSkillsInfo: specialSkillsInfo };

    const file = new Blob([JSON.stringify(sheetInfo, null, 4)], {type: 'application/json'});

    console.log(sheetInfo);
    const name = basicInfo.name;


    // Cria o link para download. Nada mais que uma âncora que
    // se refere à URL do arquivo criado e com a propriedade download
    const a = document.createElement('a');
    a.href = URL.createObjectURL(file);
    a.download = 'Ficha-' + formatToFileName(name) + '.json';
    a.click();

    // Revoga a URL. Torna o blob inacessível [eu acho].
    // Basicamente limpeza [eu acho].
    URL.revokeObjectURL(a.href);
}

// Retorna as informações básicas: nome, idade e espécie em um objeto.
function saveBasicInfo() {

    const name    = document.getElementById('name').value;
    const age     = document.getElementById('age').value;
    const species = document.getElementById('species').value;

    const basicInfo = { name: name, age: age, species: species };

    // console.log(JSON.stringify(basicInfo));
    return basicInfo;
}

// Retorna as informações de todos os atributos.
function saveAttributes() {

    const attrs = document.getElementsByClassName('js-attribute');

    const attrInfos = [];
    for (let i = 0; i < attrs.length; i += 1)
    {
        let data = saveAttribute(attrs[i]);
        attrInfos.push(data);
    }

    return attrInfos;
}

// Retorna as informações de um atributo: o check, valor máximo e valor atual.
function saveAttribute(attr) {

    const id = attr.id;
    const isChecked  = attr.querySelector('input[type=checkbox]').checked;
    const maxVal     = attr.querySelectorAll('.number-input')[0].value;
    const currentVal = attr.querySelectorAll('.number-input')[1].value;

    //document.getElementById(id + '-check').checked;
    //const maxVal = document.getElementById(id + '-m').value;
    // const currentVal = document.getElementById(id + '-a').value;

    const attrInfo = { isChecked: isChecked, maxValue: maxVal, currentValue: currentVal };
    const attrObj = { name: id, info: attrInfo };

    return attrObj;
    // return attrInfo;
}

// Retorna as informações da disposição: valor máximo e valor atual
function saveDispInfo() {

    const isChecked      = document.getElementById('disp-check').checked;
    const maxDispVal     = document.getElementById('disp-m').value;
    const currentDispVal = document.getElementById('disp-a').value;

    const dispInfo = { checked: isChecked, maxDispVal: maxDispVal, currentDispVal: currentDispVal };
    return dispInfo
}

// Retorna as informações da capacidade física e mental
function saveCapInfo() {

    const cafIsChecked  = document.getElementById('caf-check').checked;
    const cafMaxVal     = document.getElementById('caf-m').value;
    const cafFat        = document.getElementById('caf-f').value;

    const camIsChecked  = document.getElementById('cam-check').checked;
    const camMaxVal     = document.getElementById('cam-m').value;
    const camFat        = document.getElementById('cam-f').value;

    const cafInfo = { checked: cafIsChecked, maxVal: cafMaxVal, fat: cafFat };
    const camInfo = { checked: camIsChecked, maxVal: camMaxVal, fat: camFat };

    const capObj = [{ name: 'caf', info: cafInfo }, { name: 'cam', info: camInfo }]

    return capObj;

}

// Retorna as informações de todas as perícias.
function saveSkills() {

    const skills = document.getElementsByClassName('skill');

    const skillsInfo = [];
    for (let i = 0; i < skills.length; i += 1)
    {
        let data = saveSkill(skills[i]);
        skillsInfo.push(data);
    }

    return skillsInfo;
}

// Retorna as informações de uma perícia: o check e o valor atual.
function saveSkill(skill) {

    const id = skill.id;
    const isChecked  = document.getElementById(id + '-check').checked;
    const val        = document.getElementById(id + '-v').value;

    const skillInfo = { checked: isChecked, val: val };
    const skillObj = { name: id, info: skillInfo };

    return skillObj;
}

// Retorna o valor do reflexo e atenção
function saveStats() {
    const atcVal = document.getElementById('atc-v').value;
    const rflVal = document.getElementById('rfl-v').value;

    const StatsInfo = { atcVal: atcVal, rflVal: rflVal };
    return StatsInfo;
}


// Retorna as informações sobre feridas: check, resistência, feridas atuais
// e o texto descrevendo os efeitos.
function saveWounds() {

    const isChecked = document.getElementById('res-check').checked;
    const resVal    = document.getElementById('res-v').value;
    const wndVal    = document.getElementById('wnd-v').value;
    const wndText   = document.getElementById('wnd-text').value;


    const woundInfo = { checked: isChecked, resVal: resVal, wndVal: wndVal, wndText: wndText};
    return woundInfo;
}




// Retorna as informações sobre as perícias especiais
function saveSpecialSkills() {

    const listHolder = document.getElementById('specialSkillsList');
    const skillList = listHolder.querySelectorAll('.skill--special');

    const specialSkillsInfo = [];
    for (let i = 0; i < skillList.length; i += 1)
    {
        let data = saveSpecialSkill(skillList[i]);
        specialSkillsInfo.push({ id: skillList[i].id, data: data });
    }

    return specialSkillsInfo;
}


// Salva uma perícia especial e retorna as informações em um objeto.
function saveSpecialSkill(skill) {

    const check = skill.querySelector('input[type=checkbox]').checked;
    const val = skill.querySelector('input.number-input').value;
    const name = skill.querySelector('span').textContent;

    return { name: name, value: val, isChecked: check }
}
