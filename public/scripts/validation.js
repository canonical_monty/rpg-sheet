initValidationLayer();

function initValidationLayer() {

    enforceNumberFields();
    enforceUpperBounds();
}



// Garante que apenas números possa sem inseridos nas entradas numéricas.
function enforceNumberFields() {

    const nFields = document.getElementsByClassName('number-input');
    for (let i = 0; i < nFields.length; i += 1)
    {
        nFields[i].addEventListener('input', (ev) =>
            {
                let val = ev.target.value;
                if (isNaN(val) || val == ' ') { ev.target.value = ''; };
            })
    }
}



// Garante que não seja possível exceder o valor máximo em entradas 
// numéricas limitadas superiormente.
function enforceUpperBounds() {

    const bounded = document.getElementsByClassName('upper-bounded');
    for (let i = 0; i < bounded.length; i += 1)
    {
        bounded[i].addEventListener('input', (ev) =>
            {
                const val = ev.target.value;
                ev.target.value = limitMaxValue(val, 20);

                // Todos os campos numéricos são validados de modo a só
                // conterem valores numéricos ou serem vazios. Por isto
                // não há necessidade de validar val antes de usar limitMaxValue.
            })
    }
}

// Limita o valor máximo de uma entrada numérica. Não verifica se a string 
// corresponde a um valor numérico. Garantir isto antes de usar esta função.
function limitMaxValue(val, max) {

    if (val != '')
    {
        if (parseInt(val) > max) return max;
    }

    return val;
}
