addSpecialSkillBtn();
addCharacteristicBtn();


// Botão de adicionar traços
function addCharacteristicBtn() {

    const addBtn = document.getElementById('addCharacteristicBtn');
    let characteristicNumber = 0;
    
    addBtn.addEventListener('click', () =>
        {
            const characteristicList = document.getElementById('characteristicsList');

            let characteristicName = document.getElementById('characteristicName').value;
            if (!(characteristicName === ''))
            {
                characteristicNumber += 1;
                let wrapper = document.createElement('div');
                wrapper.setAttribute('class', 'flex flex--start flex--align-center');
                wrapper.setAttribute('id', 'chr-' + characteristicNumber);
                wrapper.appendChild(document.createTextNode(characteristicName));

                characteristicList.appendChild(wrapper);
            }
        });
}



// Botão de adicionar perícias especiais
function addSpecialSkillBtn() {

    const addBtn = document.getElementById('addSkillBtn');
    let skillNumber = 0;
    
    addBtn.addEventListener('click', () =>
        {
            const skillList = document.getElementById('specialSkillsList');

            let skillName = document.getElementById('specialSkillName').value;
            if (!(skillName === ''))
            {
                skillNumber += 1;
                let skillElement = addSkill(skillName, 'spc-skl-' + skillNumber);
                skillList.appendChild(skillElement);
            }

            // const popup = document.getElementById('special-skill-popup');
            // popup.classList.add('active');

            // popup.addEventListener('click',  popupListener);
        });
}



// Adiciona perícias à lista de perícias
function addSkill(name, id) {

    // Rótulo da perícias
    let label = document.createElement('span');
    label.setAttribute('class', 'input-label font-medium');
    label.appendChild(document.createTextNode(name));


    // Checkbox
    let checkboxFill = document.createElement('div');
    checkboxFill.setAttribute('class', 'checkbox-fill');

    let checkboxInput = document.createElement('input');
    checkboxInput.setAttribute('type', 'checkbox');
    checkboxInput.setAttribute('id', id + '-check');
    checkboxInput.setAttribute('name', id + '-check');
    checkboxInput.setAttribute('class', 'checkbox-input');

    let checkboxLabel = document.createElement('label');
    checkboxLabel.setAttribute('class', 'checkbox right-margin-gap');
    checkboxLabel.appendChild(checkboxInput);
    checkboxLabel.appendChild(checkboxFill);


    // Div que contem a checkbox e o nome
    let leftWrapper = document.createElement('div');
    leftWrapper.setAttribute('class', 'flex flex--start flex--align-center');
    leftWrapper.appendChild(checkboxLabel);
    leftWrapper.appendChild(label);


    // Valor numérico
    let skillVal = document.createElement('input');
    skillVal.setAttribute('class', 'number-input two-numbers');
    skillVal.setAttribute('id', id + '-v');
    skillVal.setAttribute('name', id + '-v');
    skillVal.setAttribute('maxlength', '2');

    let skillValLabel = document.createElement('label');
    skillValLabel.appendChild(skillVal);


    // Div externo
    let skillElement = document.createElement('div');
    skillElement.setAttribute('class', 'flex flex--spread flex--align-center skill--special');
    skillElement.setAttribute('id', id);
    skillElement.appendChild(leftWrapper);
    skillElement.appendChild(skillValLabel);


    return skillElement;
}





// Função do popup 
// function popupListener(ev) {
// 
//     const popup = document.getElementById('special-skill-popup');
//     const conOption = document.getElementById('special-skill-con');
//     const hisOption = document.getElementById('special-skill-his');
// 
//     const skillList = document.getElementById('specialSkillsList');
//     if (ev.target == conOption) 
//     {
//         let skillElement = addSkill('spc', 'Conhecimento');
//         skillList.appendChild(skillElement);
//     }
//     else if (ev.target == hisOption) 
//     {
//         let skillElement = addSkill('spc', 'História');
//         skillList.appendChild(skillElement);
//     }
//     else if (ev.target == popup)
//     {
//         popup.classList.remove('active');
//         popup.removeEventListener('click', popupListener);
//     }
// 
//     console.log(ev.target);
// }



