turnOnButtons();

function turnOnButtons() {

    barSelectorBtns();
    clearSheetBtn();
    rngGenerationBtn();
    rngSkillGenerationBtn();
}



/* Botões para selecionar as barras laterais direitas */
function barSelectorBtns() {

    const barSelector = document.getElementById('bar-selector');

    const utilityBarBtn = document.getElementById('utils-btn');
    const utilityBar = document.getElementById('utility-bar');

    const extBarBtn = document.getElementById('ext-btn');
    const extBar = document.getElementById('ext-bar');

    barSelector.addEventListener('click', (ev) =>
        {
            if (ev.target == utilityBarBtn)
            {
                extBar.classList.remove('active');
                barSelector.classList.remove('ext-active');

                utilityBar.classList.toggle('active');
                barSelector.classList.toggle('utils-active');
            }
            else if (ev.target == extBarBtn)
            {
                utilityBar.classList.remove('active');
                barSelector.classList.remove('utils-active');

                extBar.classList.toggle('active');
                barSelector.classList.toggle('ext-active');
            }

        });
}



// Botão para apagar toda a ficha
function clearSheetBtn() {
    const clearBtn = document.getElementById('clear-btn');
    clearBtn.addEventListener('click', () =>
        {
            const fields = document.getElementsByTagName('input');
            for (let i = 0; i < fields.length; i += 1)
            {
                if (fields[i].type == 'text') { fields[i].value = ''; }
                else if (fields[i].type == 'checkbox') 
                {
                    // Reseta todas as checkbox exceto o botão de menu
                    if (fields[i].id != 'main-menu-btn') { fields[i].checked = false; }
                }
            }

            const textareas = document.getElementsByTagName('textarea');
            for (let i = 0; i < textareas.length; i += 1)
            {
                textareas[i].value = '';
            }
        });
}



// Botão para gerar atributos aleatórios
function rngGenerationBtn() {
    const rngGenBtn = document.getElementById('gen-attr-btn');
    rngGenBtn.addEventListener('click', () =>
        {
            const attrs = document.getElementsByClassName('js-attribute');
            for (let i = 0; i < attrs.length; i += 1)
            {
                const attrId = attrs[i].id;
                const maxAttr     = document.getElementById(attrId + '-m');
                const currentAttr = document.getElementById(attrId + '-a');

                // Rola 4D4
                let diceResult = 0;
                for (let j = 0; j < 4; j += 1) { diceResult += Math.floor(4 * Math.random()) + 1; }

                // Limita o resultado para um máximo de 14, o valor máximo inicial.
                const maxInitAttr = 14
                if (diceResult > maxInitAttr) { diceResult = maxInitAttr;}

                maxAttr.value     = diceResult;
                currentAttr.value = diceResult;

                // Dispara um evento customizado 'generated' para ativar
                // as funcionalidades de automação da ficha e preenche-la.
                const genEv = new Event('generated', { bubbles: true });
                currentAttr.dispatchEvent(genEv);
            }
        });
}


// Botão para gerar perícias
function rngSkillGenerationBtn() {

    const rngSkillGenBtn = document.getElementById('gen-skill-btn');
    rngSkillGenBtn.addEventListener('click', () => { generateSkills(); });
}


// Gera o valor das perícias de fato
function generateSkills() {

    // Converte a lista de perícias em um array propriamente.
    // let skillList = [... document.getElementsByClassName('skill')];
    
    // Coleta os nomes de todas as perícias
    let skillList = [];

    const skillElementList = document.getElementsByClassName('skill');
    for (let i = 0; i < skillElementList.length; i += 1)
    {
        skillList.push(skillElementList[i].id);
    }

    // Limpa o valor de todas as perícias. Necessário porque nem todas
    // as perícias são preenchidas. Se não forem apagadas usar este 
    // botão consecutivamente gerará mais perícias que o número inicial.
    for (let i = 0; i < skillList.length; i += 1)
    {
        document.getElementById(skillList[i] + '-v').value = '';
    }


    // Número inicial de perícias
    let initSkillNumber = 6;
    for ( let n = 0; n < initSkillNumber; n += 1)
    {
        // Escolhe aleatoriamente uma perícia
        let index = Math.floor(skillList.length * Math.random());

        // Rola o valor da perícia, 3D4
        let diceResult = 0;
        for (let j = 0; j < 2; j += 1) { diceResult += Math.floor(4 * Math.random()) + 1; }

        // Preenche o valor da perícia
        document.getElementById(skillList[index] + '-v').value = diceResult;

        // Remove o elemento da lista
        skillList = removeElement(skillList, index);
    }

}



// Remove o elemento do índice indicado no array
function removeElement(array, index) {

    if ( index < array.length )
    {
        return array.slice(0, index).concat(array.slice(index + 1));
    }
    else
    {
        return array;
    }
}







