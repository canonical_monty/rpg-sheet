turnOnAutomation();

function turnOnAutomation() {

    watchMaxAttributes();
    watchAttributes();

    HandleDispInfo();
    HandleResInfo();
    HandleCafInfo();
    HandleCamInfo();
}



// Funções relativas a atributos

//// Lida com alterações nos valores máximos dos atributos
function watchMaxAttributes() {

    const attrs = document.getElementsByClassName('js-attribute');
    for (let i = 0; i < attrs.length; i += 1)
    {
        const attrId = attrs[i].id;
        const maxAttr     = document.getElementById(attrId + '-m');
        const currentAttr = document.getElementById(attrId + '-a');
        
        // Preenche o valor atual do atributo se estiver vazio.
        maxAttr.addEventListener('change', () => 
            { 
                fillIfBlank(maxAttr, currentAttr, true);
                equateIfGreater(maxAttr, currentAttr, true);
            });
    }
}


//// Lida com alterações nos valores atuais dos atributos
function watchAttributes() {

    const attrAgi = document.getElementById('agi-a');
    const attrCon = document.getElementById('con-a');
    const attrDet = document.getElementById('det-a');
    const attrFoc = document.getElementById('foc-a');
    const attrFor = document.getElementById('for-a');
    const attrPer = document.getElementById('per-a');


    document.body.addEventListener('change', (ev) =>
        {
            if (ev.target == attrAgi) { HandleAgi(attrAgi, false); }
            else if (ev.target == attrCon) { HandleCon(attrCon, false); }
            else if (ev.target == attrDet) { HandleDet(attrDet, false); }
            else if (ev.target == attrFoc) { HandleFoc(attrFoc, false); }
            else if (ev.target == attrFor) { HandleFor(attrFor, false); }
            else if (ev.target == attrPer) { HandlePer(attrPer, false); }
        });


    // Lida com o caso particular da geração de atributos aleatoriamente
    // por meio do evento personalizado 'generated'
    document.body.addEventListener('generated', (ev) =>
        {
            if (ev.target == attrAgi) { HandleAgi(attrAgi, true); }
            else if (ev.target == attrCon) { HandleCon(attrCon, true); }
            else if (ev.target == attrDet) { HandleDet(attrDet, true); }
            else if (ev.target == attrFoc) { HandleFoc(attrFoc, true); }
            else if (ev.target == attrFor) { HandleFor(attrFor, true); }
            else if (ev.target == attrPer) { HandlePer(attrPer, true); }
        });

}

//// Lida com alterações no valor atual da agilidade
function HandleAgi(attrAgi, isGenerated) {

    if (attrAgi.value != '')
    {
        // Recalcula a capacidade física
        const attrCon = document.getElementById('con-a');
        const attrFor = document.getElementById('for-a');
        const cafVal = document.getElementById('caf-m');
        calcCapacity(cafVal, attrAgi, attrCon, attrFor)


        // Recalcula o reflexo
        calcReflex(attrAgi);
    }
}


//// Lida com alterações no valor atual da constituição
function HandleCon(attrCon, isGenerated) {

    if (attrCon.value != '')
    {
        // Recalcula a capacidade física
        const attrAgi = document.getElementById('agi-a');
        const attrFor = document.getElementById('for-a');
        const cafVal = document.getElementById('caf-m');
        calcCapacity(cafVal, attrAgi, attrCon, attrFor)


        // Recalcula o disposição
        calcDisp(attrCon, false);

        // Recalcula o resistência
        calcRes(attrCon);

        if (isGenerated) { calcDisp(attrCon, true); }
    }
}

//// Lida com alterações no valor atual da determinação
function HandleDet(attrDet, isGenerated) {

    if (attrDet.value != '')
    {
        // Recalcula a capacidade mental
        const attrFoc = document.getElementById('foc-a');
        const attrPer = document.getElementById('per-a');
        const camVal = document.getElementById('cam-m');
        calcCapacity(camVal, attrDet, attrFoc, attrPer)
    }
}


//// Lida com alterações no valor atual do foco
function HandleFoc(attrFoc, isGenerated) {

    if (attrFoc.value != '')
    {
        // Recalcula a capacidade mental
        const attrDet = document.getElementById('det-a');
        const attrPer = document.getElementById('per-a');
        const camVal = document.getElementById('cam-m');
        calcCapacity(camVal, attrDet, attrFoc, attrPer)
    }
}


//// Lida com alterações no valor atual da força
function HandleFor(attrFor, isGenerated) {

    if (attrFor.value != '')
    {
        // Recalcula a capacidade física
        const attrAgi = document.getElementById('agi-a');
        const attrCon = document.getElementById('con-a');
        const cafVal = document.getElementById('caf-m');
        calcCapacity(cafVal, attrAgi, attrCon, attrFor)
    }
}

//// Lida com alterações no valor atual da percepção
function HandlePer(attrPer, isGenerated) {

    if (attrPer.value != '')
    {
        // Recalcula a capacidade mental
        const attrDet = document.getElementById('det-a');
        const attrFoc = document.getElementById('foc-a');
        const camVal = document.getElementById('cam-m');
        calcCapacity(camVal, attrDet, attrFoc, attrPer)


        // Recalcula a atenção
        calcAttention(attrPer);
    }
}







// Funções relativas à disposição

//// Calcula a disposição máxima a partir da constituição atual.
function calcDisp(attrCon, current) {

    if (!current)
    {
        const maxDisp = document.getElementById('disp-m');
        maxDisp.value = 2 * parseInt(attrCon.value);

        // Manda um envento 'change' para que o valor da barra de disposição seja recalculado.
        const changeEv = new Event('change');
        maxDisp.dispatchEvent(changeEv);
    }
    else
    {
        const currentDisp = document.getElementById('disp-a');
        currentDisp.value = 2 * parseInt(attrCon.value);
    }
}


//// Monitora os campos relacionados à disposição.
function HandleDispInfo() {

    const lifeBar     = document.getElementById('disp-bar');
    const dispCheck   = document.getElementById('disp-check');
    const maxDisp     = document.getElementById('disp-m');
    const currentDisp = document.getElementById('disp-a');


    // Marca o check da disposição quando ela chega a zero.
    currentDisp.addEventListener('change', () =>
        {
            if (currentDisp.value == '0') { dispCheck.checked = true; }
        });
    

    maxDisp.addEventListener('change', () => 
        {
            // Preenche a disposição atual com o valor máximo se ela estiver vazia
            fillIfBlank(maxDisp, currentDisp, false);

            // Reduz o valor da disposição atual se ela exceder o valor máximo após a alteração
            equateIfGreater(maxDisp, currentDisp, false);

            // Preenche a barra de disposição e atualiza quando a disposição máxima é alterada
            fillBar(lifeBar, maxDisp.value, true);
        });

}


// Funções relativas à resistência e feridas

//// Calcula a resistência a partir da constituição atual.
function calcRes(attrCon) {

    const resVal   = document.getElementById('res-v');
    resVal.value = parseInt(attrCon.value);
}


//// Monitora os campos relacionados à resistência e feridas.
function HandleResInfo() {

    const wndVal   = document.getElementById('wnd-v');
    const resVal   = document.getElementById('res-v');
    const resCheck = document.getElementById('res-check');

    // Marca o check da resistência quando as feridas passam da metade
    // do valor da resistência atual.
    wndVal.addEventListener('change', () =>
        {
            if (resVal.value != '' && wndVal.value != '')
            {
                let rv = parseInt(resVal.value);
                let wv = parseInt(wndVal.value);

                if (wv > Math.floor(rv / 2)) { resCheck.checked = true; }
            }
        });

}


// Funções relativas às capacidades física e mental

//// Monitora os campos relativos à capacidade física
function HandleCafInfo() {

    const cafVal   = document.getElementById('caf-m');
    const fatVal   = document.getElementById('caf-f');
    const cafCheck = document.getElementById('caf-check');

    // Marca o check da capacidade física quando a fatiga passa da metade
    // do valor da capacidade atual.
    fatVal.addEventListener('change', () =>
        {
            if (fatVal.value != '' && cafVal.value != '')
            {
                let cv = parseInt(cafVal.value);
                let fv = parseInt(fatVal.value);

                if (fv > Math.floor(cv / 2)) { cafCheck.checked = true; }
            }
        });

}


//// Monitora os campos relativos à capacidade mental
function HandleCamInfo() {

    const camVal   = document.getElementById('cam-m');
    const fatVal   = document.getElementById('cam-f');
    const camCheck = document.getElementById('cam-check');

    // Marca o check da capacidade física quando a fatiga passa da metade
    // do valor da capacidade atual.
    fatVal.addEventListener('change', () =>
        {
            if (fatVal.value != '' && camVal.value != '')
            {
                let cv = parseInt(cafVal.value);
                let fv = parseInt(fatVal.value);

                if (fv > Math.floor(cv / 2)) { camCheck.checked = true; }
            }
        });

}


//// Calcula uma capacidade, física ou mental, a partir de três atributos.
function calcCapacity(cap, attr1, attr2, attr3) {

    if ( attr1.value != '' && attr2.value != '' && attr3.value != '')
    {
        let total = parseInt(attr1.value) + parseInt(attr2.value) + parseInt(attr3.value);
        cap.value = Math.floor(total / 6);
    }
}




// Calcula o atenção
function calcAttention(attrPer) {

    const atcVal = document.getElementById('atc-v');
    const perVal = parseInt(attrPer.value);

    if (perVal <= 10) { atcVal.value = 1; }
    else if (perVal <= 16) { atcVal.value = 2; }
    else { atcVal.value = 3; }
}



// Calcula o reflexo
function calcReflex(attrAgi) {

    const rflVal = document.getElementById('rfl-v');
    const agiVal = parseInt(attrAgi.value);

    if (agiVal <= 10) { rflVal.value = 1; }
    else if (agiVal <= 16) { rflVal.value = 2; }
    else if (agiVal <= 18) { rflVal.value = 3; }
    else { rflVal.value = 4; }
}



// Utilidades

//// Esta função iguala o valor de target ao valor de source
//// se o primeiro estiver vazio. O parâmetro booleano
//// shouldFire determina se esta alteração deve lançar um
//// envento do tipo change ou não.
function fillIfBlank(source, target, shouldFire) {

    if (target.value === '') 
    { 
        target.value = source.value;

        // Manda um envento 'change' cuja fonte é o valor recem alterado.
        if (shouldFire) 
        {
            const changeEv = new Event('change', { bubbles: true });
            target.dispatchEvent(changeEv);
        }
    }
}


//// Esta função iguala o valor de target ao valor de source
//// se este for maior que o primeiro. O parâmetro booleano
//// shouldFire determina se esta alteração deve lançar um
//// envento do tipo change ou não.
function equateIfGreater(source, target, shouldFire) {

    if (!(target.value === ''))
    { 
        if (parseInt(target.value) > parseInt(source.value))
        {
            target.value = source.value;

            // Manda um envento 'change' cuja fonte é o valor recem alterado.
            if (shouldFire) 
            {
                const changeEv = new Event('change', { bubbles: true });
                target.dispatchEvent(changeEv);
            }
        }
    }
}




//// Função que preenche barras. Toma como argumento a barra a ser preenchida,
//// o valor usado como base e uma parâmetro booleano para determinar se as
//// entradas são múltiplos ou divisões do valor base.
function fillBar(bar, baseVal, div) {

    if (baseVal != '')
    {
        const inputList = bar.querySelectorAll('input');
        for (let i = 0; i < inputList.length; i += 1)
        {
            if (div == true) 
            { 
                // Se a barra for de divisão, como é o caso da barra de disposição, cada entrada
                // é multiplicada por um múltiplo inteiro de 1/número de entradas. Com quatro 
                // entradas tem-se 1/4, 1/2 3/4 1, por exemplo.
                inputList[i].value = Math.floor(parseInt(baseVal) * ((i + 1) / inputList.length)); 
            }

            // Se a barra for multiplicativa, simplesmente calcula os múltiplos inteiros
            // do valor base de acordo com o número da entrada. A primeira entrada é 
            // multiplicada por 1, a segunda por 2 e assim por diante.
            else { inputList[i].value = parseInt(baseVal) * (i + 1); }
        }
    }
}
