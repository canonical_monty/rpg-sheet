// Isso funciona como deveria, mas não sei como ler um arquivo json 
// para pegar a lista de perícias e colocar aqui. Então tenho que 
// fazer manualmente. Se o arquivo não for selecionado pelo navegador
// como é o caso do botão de carregar, então acho que tem que ser 
// por meio de HTTP e variações, o que não funcion no gitlab.
// 
// De todo modo essa função vai ser útil depois.

// addSkill('fur', 'Furtividade');
// addSkill('esc', 'Escalar');
// addSkill('arr', 'Arrombar');
// addSkill('slt', 'Saltar');
// addSkill('mnt', 'Mentir');
// addSkill('snd', 'Sondar');
// addSkill('rst', 'Rastrear');
// addSkill('ndr', 'Nadar');
// addSkill('med', 'Medicina');
// addSkill('atf', 'Arte da fuga');
// addSkill('flg', 'Folego');
// addSkill('hrb', 'Herbalismo');




// Adiciona perícias à lista de perícias
function addSkill(id, name) {

    let skillBox = document.getElementById('skill-box');


    let label = document.createElement('label');
    label.setAttribute('class', 'field-label');
    label.setAttribute('for', id + '-v');
    label.appendChild(document.createTextNode(name));
        

    let innerDiv = document.createElement('div');
    innerDiv.setAttribute('class', 'inline-element');
    innerDiv.appendChild(createCheck(id));
    innerDiv.appendChild(label);

    let valInput = document.createElement('input');
    valInput.setAttribute('class', 'number-input upper-bounded');
    valInput.setAttribute('name', id + '-v');
    valInput.setAttribute('id', id + '-v');
    valInput.setAttribute('maxlength', '2');
    valInput.setAttribute('required', '');



    let perBox = document.createElement('div');
    perBox.setAttribute('class', 'inline-element full-width skill');
    perBox.setAttribute('id', id);
    perBox.appendChild(innerDiv);
    perBox.appendChild(valInput);


    skillBox.appendChild(perBox);
    // console.log(skillBox.outerHTML);

}

function createCheck(id) {

    // Namespace de svg
    svgName = 'http://www.w3.org/2000/svg';

    let squareRef = document.createElementNS(svgName, 'use');
    squareRef.setAttribute('class', 'checkbox-mark');
    squareRef.setAttribute('x', '0');
    squareRef.setAttribute('y', '0');
    squareRef.setAttribute('href', '#square-fill');


    let squareSVG = document.createElementNS(svgName, 'svg');
    squareSVG.setAttribute('class', 'checkbox-mark');
    squareSVG.setAttribute('viewBox', '0 0 32 32'); 
    squareSVG.setAttribute('xmlns', 'http://www.w3.org/2000/svg');
    squareSVG.setAttribute('xmlns:xlink', 'http://www.w3.org/1999/xlink');
    squareSVG.appendChild(squareRef);


    let checkboxFill = document.createElement('div');
    checkboxFill.setAttribute('class', 'checkbox-fill');
    checkboxFill.appendChild(squareSVG);

    let checkboxInput = document.createElement('input');
    checkboxInput.setAttribute('id', id + '-check');
    checkboxInput.setAttribute('type', 'checkbox');



    let checkboxInputWrapper = document.createElement('div');
    checkboxInputWrapper.setAttribute('class', 'checkbox-input');
    checkboxInputWrapper.appendChild(checkboxInput);
    checkboxInputWrapper.appendChild(checkboxFill);



    let checkbox = document.createElement('div');
    checkbox.setAttribute('class', 'checkbox');
    checkbox.appendChild(checkboxInputWrapper);


    let checkboxWrapper = document.createElement('label');
    checkboxWrapper.setAttribute('class', 'checkbox-wrapper checkbox-gap');
    checkboxWrapper.setAttribute('for', id + '-check');
    checkboxWrapper.appendChild(checkbox);


    return checkboxWrapper;
}

