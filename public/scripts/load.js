turnOnLoadBtn();

// Funcionalidade do botão de carregar
function turnOnLoadBtn() {

    const loadBtn = document.getElementById('load-btn');
    loadBtn.addEventListener('change', (ev) => { load(ev) });
}



// Carrega as informações de um arquivo e preenche a ficha.
function load(ev) {

    // console.log('Carregando ficha');

    const loadedFiles = ev.target.files;
    if (loadedFiles.lenght == 0) console.log('Nenhum arquivo carregado');
    else {

        // console.log('Ficha carregada');
        const infoFile = loadedFiles[0];

        // Leitor de arquivos
        var reader = new FileReader();

        // Quando o arquivo termina de ser lido, dispara o evento loadend.
        // Se a leitura tiver sido bem sucedida, dispara load e se não, error.
        // A propriedade reader.onload permite definir o que acontece quando
        // o arquivo termina de ser carregado.
        reader.readAsText(infoFile);

        reader.onload = (e) => {
            // console.log(e);

            const infos = JSON.parse(e.target.result);

            console.log(infos);
            loadBasicInfo(infos.basicInfo);
            loadAttrs(infos.attrInfo);
            loadDisp(infos.dispInfo);
            loadStats(infos.statsInfo);
            loadCaps(infos.capInfo);
            loadWounds(infos.woundInfo);
            loadSkills(infos.skillsInfo);
            loadSpecialSkills(infos.specialSkillsInfo);
        };
    }
}

// Carrega as informações básicas: nome, idade, espécie
function loadBasicInfo(basicInfo) {

    document.getElementById('name').value    = basicInfo.name;
    document.getElementById('age').value     = basicInfo.age;
    document.getElementById('species').value = basicInfo.species;
}


// Carrega as informações de todos os atributos.
function loadAttrs(attrInfo) {

    for (let i = 0; i < attrInfo.length; i += 1)
    {
        attrName = attrInfo[i].name;
        loadAttr(attrName, attrInfo[i].info);
    }
}

// Carrega as informações de um atributo: a check, valor máximo e valor atual.
function loadAttr(attrName, attrData) {

    document.getElementById(attrName + '-check').checked = attrData.isChecked;
    document.getElementById(attrName + '-m').value       = attrData.maxValue; 
    document.getElementById(attrName + '-a').value       = attrData.currentValue;
}


// Carrega as informações da disposição: check, valor máximo e valor atual.
function loadDisp(dispInfo) {

    document.getElementById('disp-check').checked = dispInfo.checked;
    document.getElementById('disp-m').value       = dispInfo.maxDispVal; 
    document.getElementById('disp-a').value       = dispInfo.currentDispVal;

    // Manda um envento 'change' para que o valor da barra de disposição seja recalculado.
    const changeEv = new Event('change');
    document.getElementById('disp-m').dispatchEvent(changeEv);
}


// Carrega as informações das capacidade física e mental.
function loadCaps(capInfo) {

    for (let i = 0; i < capInfo.length; i += 1)
    {
        capName = capInfo[i].name;
        loadCap(capName, capInfo[i].info);
    }
}


// Carrega as informações de uma capcaidade: a check, valor máximo e valor atual.
function loadCap(capName, capData) {

    document.getElementById(capName + '-check').checked = capData.checked;
    document.getElementById(capName + '-m').value       = capData.maxVal; 
    document.getElementById(capName + '-f').value       = capData.fat;
}



// Carrega o valor do reflexo e atenção
function loadStats(statsInfo) {
    document.getElementById('atc-v').value = statsInfo.atcVal;
    document.getElementById('rfl-v').value = statsInfo.rflVal;
}



// Carrega as informações sobre feridas: check, resistência, feridas atuais
// e o texto descrevendo os efeitos.
function loadWounds(woundInfo) {

    document.getElementById('res-check').checked = woundInfo.checked;
    document.getElementById('res-v').value       = woundInfo.resVal;
    document.getElementById('wnd-v').value       = woundInfo.wndVal;
    document.getElementById('wnd-text').value    = woundInfo.wndText;
}



// Carrega as informações de todas as perícias.
function loadSkills(skillsInfo) {

    for (let i = 0; i < skillsInfo.length; i += 1)
    {
        skillName = skillsInfo[i].name;
        loadSkill(skillName, skillsInfo[i].info);
    }
}

// Carrega as informações de uma perícia: o check e o valor.
function loadSkill(skillName, skillData) {

    document.getElementById(skillName + '-check').checked = skillData.checked;
    document.getElementById(skillName + '-v').value       = skillData.val; 
}



// Carrega as informações de todas as perícias especiais
function loadSpecialSkills(specialSkillsInfo) {

    // Lista de perícias especiais
    const skillList = document.getElementById('specialSkillsList');

    // Remove quaisquer perícias presentes antes de carregar
    const skills = skillList.querySelectorAll('.skill--special');
    for (let i = skills.length - 1; i >= 0; i -= 1) { skills[i].remove(); }

    //Loop sobre as perícias guardadas
    for (let i = 0; i < specialSkillsInfo.length; i += 1)
    {
        let specialSkill = addSkill(specialSkillsInfo[i].data.name, specialSkillsInfo[i].id);
        loadSpecialSkill(specialSkill, specialSkillsInfo[i].data);

        skillList.appendChild(specialSkill);
    }
}

// Carrega as informações de uma perícia: o check e o valor.
function loadSpecialSkill(specialSkill, specialSkillData) {

    specialSkill.querySelector('input[type=checkbox').checked = specialSkillData.isChecked;
    specialSkill.querySelector('input.number-input').value    = specialSkillData.value; 
}
